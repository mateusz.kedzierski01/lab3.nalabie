<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>
<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:import url="ludzie.xml" var="baza" />
        <x:parse doc="${baza}" var="wynik"/>

    <c:choose>
         
         <c:when test = "${logowanie.nazwa == null}">
             <h1>Brak dostępu do listy ludzi!</h1>
         </c:when>
         <c:otherwise>
             
            <ol>
            Zalogowany użytkownik: ${logowanie.nazwa}
            <x:forEach select="$wynik/ludzie/czlowiek" var="zasob" varStatus="loop">
                <li><b><x:out select="nazwisko" /></b>
                    <x:out select="imie" /></li>
            </x:forEach>
            </ol>
            <h:form>
                <h:commandButton value="Wyloguj" action="logout" />
            </h:form>
         </c:otherwise>
    </c:choose>
        
    </body>
</html>
</f:view>