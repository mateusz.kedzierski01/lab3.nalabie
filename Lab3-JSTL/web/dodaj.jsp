<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dodawanie ludzi</title>
    </head>
    <body>
        <%
            String filename =  application.getRealPath("/") + "ludzie.xml";
            BufferedReader rd = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line;
            String xml;
            int maxID=0;
            while((line = rd.readLine())!= null){
                sb.append(line+"\n");
                if(line.contains("id=")) maxID++;
            }
            
            xml = sb.toString();
            String czl = "<czlowiek id=" + (maxID+1) + ">";
            xml.replace("</ludzie>", czl);
            xml = xml.concat("<nazwisko>");
            xml = xml.concat(request.getParameter("nazwisko"));
            xml = xml.concat("</nazwisko>");
            xml = xml.concat("<imie>");
            xml = xml.concat(request.getParameter("imie"));
            xml = xml.concat("</imie>");
            xml = xml.concat("</czlowiek>");
            xml = xml.concat("</ludzie>");
        %>
    </body>
</html>
