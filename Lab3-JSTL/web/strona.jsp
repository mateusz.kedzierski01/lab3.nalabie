<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:import url="ludzie.xml" var="baza" />
        <x:parse doc="${baza}" var="wynik"/>
            <x:forEach select="$wynik/ludzie/czlowiek" var="zasob" varStatus="loop">
                    <c:if test="${loop.count == param.id}">    
                        <c:out value="${loop.count}"/>
                        <b><x:out select="nazwisko" /></b>
                        <x:out select="imie" />
                    </c:if>
            </x:forEach>
    </body>
</html>
