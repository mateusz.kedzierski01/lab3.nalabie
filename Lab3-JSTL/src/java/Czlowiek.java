import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author Mefju
 */
public class Czlowiek implements Serializable {
    
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String imie;
    private String nazwisko;
    
    private final PropertyChangeSupport propertySupport;
    
    public Czlowiek() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getImie() {
        return imie;
    }
    
    public void setPrzykladowaDana(String value) {
        String oldValue = imie;
        imie = value;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, imie);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
}
