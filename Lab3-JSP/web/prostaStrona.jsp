<%-- 
    Document   : prostaStrona
    Created on : 2020-12-15, 21:07:31
    Author     : Mefju
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="5; index.html">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="dBean" class="Lab3.DataBean" scope="session" />
        <jsp:setProperty name="dBean" property="przykladowaDana" value="<%= request.getParameter("cena")%>" />
        Zapisałem dane do Beana.<br>
        Wyprowadzam dane z Beana:
        <i><jsp:getProperty name="dBean" property="przykladowaDana" /></i><br>
        Wywołuję inną metodą Beana:
        <i><%= dBean.Dopisz("zł")%></i>
        
    </body>
</html>
